import '../scss/main.scss'

let productList = [
    {
        'img':  '80_1918_55865_ru.jpg.normal.jpg',
        'text': 'Renu Multiplus 360 ml',
        'type': 'accessories'
    },
    {
        'img':  '748_2036_56490_ru.jpg.normal.jpg',
        'text': 'Opti-Free Express 355 ml',
        'type': 'accessories'
    },
    {
        'img':  '83_1924_55899_ru.jpg.normal.jpg',
        'text': 'ReNu Multi-Purpose Solution 365 ml',
        'type': 'accessories'
    },
    {
        'img':  '84_1925_55904_ru.jpg.normal.jpg',
        'text': 'Капли Renu Multiplus 8 ml',
        'type': 'accessories'
    },
    {
        'img':  '66169.product.1007.jpg.big.jpg',
        'text': 'Johnson & Johnson 1-Day Acavue',
        'type': 'colours'
    },
    {
        'img':  '71103.product.2524.jpg.big.jpg',
        'text': 'Alcon Air Optix Colors 2 линзы',
        'type': 'colours'
    },
    {
        'img':  '70498.product.65.jpg.big.jpg',
        'text': 'Alcon FreshLook ColorBlends 1 some',
        'type': 'colours'
    },
    {
        'img':  '66744.product.4744.jpg.big.jpg',
        'text': 'Johnson & Johnson 1-Day Acavue',
        'type': 'colours'
    },
    {
        'img':  '69131.product.259.jpg.big.jpg',
        'text': 'Johnson & Johnson Acavue Oasis',
        'type': 'transparent'
    },
    {
        'img':  '67880.product.899.jpg.big.jpg',
        'text': 'Johnson & Johnson 1-Day Acavue',
        'type': 'transparent'
    },
    {
        'img':  '8218.product.521.jpg.big.jpg',
        'text': 'Johnson & Johnson 1-Day Acavue',
        'type': 'transparent'
    },
    {
        'img':  '70484.product.538.jpg.big.jpg',
        'text': 'Alcon Air Optix Aqua 6 линз',
        'type': 'transparent'
    },
];

let main = document.getElementById('main');

for (let i = 0; i < productList.length; i++) {
    let tmp = document.getElementById('product-template').content.cloneNode(true);
    let product = productList[i];

    tmp.querySelector('.product').classList.add(product.type);
    tmp.querySelector('.product__img').setAttribute('src', 'img/'+product.img);
    tmp.querySelector('.product__text').innerText = product.text;

    main.appendChild(tmp);
}
