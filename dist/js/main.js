/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _scss_main_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../scss/main.scss */ \"./src/scss/main.scss\");\n/* harmony import */ var _scss_main_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_scss_main_scss__WEBPACK_IMPORTED_MODULE_0__);\n\nvar productList = [{\n  'img': '80_1918_55865_ru.jpg.normal.jpg',\n  'text': 'Renu Multiplus 360 ml',\n  'type': 'accessories'\n}, {\n  'img': '748_2036_56490_ru.jpg.normal.jpg',\n  'text': 'Opti-Free Express 355 ml',\n  'type': 'accessories'\n}, {\n  'img': '83_1924_55899_ru.jpg.normal.jpg',\n  'text': 'ReNu Multi-Purpose Solution 365 ml',\n  'type': 'accessories'\n}, {\n  'img': '84_1925_55904_ru.jpg.normal.jpg',\n  'text': 'Капли Renu Multiplus 8 ml',\n  'type': 'accessories'\n}, {\n  'img': '66169.product.1007.jpg.big.jpg',\n  'text': 'Johnson & Johnson 1-Day Acavue',\n  'type': 'colours'\n}, {\n  'img': '71103.product.2524.jpg.big.jpg',\n  'text': 'Alcon Air Optix Colors 2 линзы',\n  'type': 'colours'\n}, {\n  'img': '70498.product.65.jpg.big.jpg',\n  'text': 'Alcon FreshLook ColorBlends 1 some',\n  'type': 'colours'\n}, {\n  'img': '66744.product.4744.jpg.big.jpg',\n  'text': 'Johnson & Johnson 1-Day Acavue',\n  'type': 'colours'\n}, {\n  'img': '69131.product.259.jpg.big.jpg',\n  'text': 'Johnson & Johnson Acavue Oasis',\n  'type': 'transparent'\n}, {\n  'img': '67880.product.899.jpg.big.jpg',\n  'text': 'Johnson & Johnson 1-Day Acavue',\n  'type': 'transparent'\n}, {\n  'img': '8218.product.521.jpg.big.jpg',\n  'text': 'Johnson & Johnson 1-Day Acavue',\n  'type': 'transparent'\n}, {\n  'img': '70484.product.538.jpg.big.jpg',\n  'text': 'Alcon Air Optix Aqua 6 линз',\n  'type': 'transparent'\n}];\nvar main = document.getElementById('main');\n\nfor (var i = 0; i < productList.length; i++) {\n  var tmp = document.getElementById('product-template').content.cloneNode(true);\n  var product = productList[i];\n  tmp.querySelector('.product').classList.add(product.type);\n  tmp.querySelector('.product__img').setAttribute('src', 'img/' + product.img);\n  tmp.querySelector('.product__text').innerText = product.text;\n  main.appendChild(tmp);\n}\n\n//# sourceURL=webpack:///./src/js/index.js?");

/***/ }),

/***/ "./src/scss/main.scss":
/*!****************************!*\
  !*** ./src/scss/main.scss ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("// removed by extract-text-webpack-plugin\n\n//# sourceURL=webpack:///./src/scss/main.scss?");

/***/ })

/******/ });